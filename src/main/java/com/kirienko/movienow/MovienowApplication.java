package com.kirienko.movienow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MovienowApplication {

	public static void main(String[] args) {
		SpringApplication.run(MovienowApplication.class, args);
	}

}

